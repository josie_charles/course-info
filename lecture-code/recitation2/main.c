#include<stdio.h>
#include <string.h>
#include <stdlib.h>

// Header File - directory of functions available in a library
// Library - compiled functions that can be used in your code
// - .so
// - .a
/* This is a block comment 
*/

int main(int argc, char** argv) {

  /*
  argc = 5
  argv[0] = "which"
  argv[1] = "-a"
  argv[2] = "foo"
  argv[3] = "bar"
  argv[4] = "baz"
  */
  int a_flag = 0;
  int return_value = 0;

  /*
    if (strcmp(argv[i],"-a") == 0) {
      // We have a -a flag!
      printf("We found a -a!\n");
      a_flag = 1;
    } else {
      if (argv[i][0] == '-') {
        printf("We have an invalid flag\n");
      } else {
        printf("The filename we need to find is: %s\n", argv[i]);
      }
      // Either:
      // 1. We have an unknown flag
      // 2. We have a filename
    }
  }*/

  printf("The PATH is: %s\n", getenv("PATH"));
  
  // str split or find
  // str concatenate
  // directory: /home/cmontella/.cargo/bin
  // filename: foo
  // full path to file: /home/cmontella/.cargo/bin/foo
  // access("/home/cmontella/.cargo/bin/foo")

  return 0;
}
